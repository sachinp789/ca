<?php
/**
 * Template Name: Contact
 *
 * @package WordPress
 * @subpackage Capatel
 * @since Capatel 1.0
 */
get_header();
$pageID = get_the_ID();
$content_post = get_post($pageID);
$image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()), 'full' );
?>
<div class="container">
    <div class="col-12 mx-auto text-center">
    </div>
</div>
<section class="inner-banner-section">
    <img src="<?php echo $image?>">
    <div class="banner">
        <div class="section-main-title">
            <?php _e(get_the_Title())?>
        </div>
    </div>
    <div class="banner-line"></div>
</section>
<section class="contact-section bg-body">
    <div class="container">
        <div class="row">
            <div class="col-8 wow fadeInLeft mx-auto">
                <div class="contact-form">
                    <?php _e(do_shortcode($content_post->post_content));?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="bg-body">
    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d14687.341391446307!2d72.598616!3d23.029817!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xe2f887f25e2f066!2sRaj+Patel+-+C.A.Patel+Textiles+Private+Limited!5e0!3m2!1sen!2sin!4v1533285398455" width="100%" height="480" frameborder="0" style="border:0" allowfullscreen></iframe>
</section>
<?php
get_footer();
?>
<script type="text/javascript">
(function () {
    $(window).scroll(function () {
        if (isScrolledIntoView('.inner-banner-section')) {
        $('.inner-banner-section .section-main-title').addClass('animate-line');
        $('.inner-banner-section .section-main-title').addClass('animate-line');
    }
        if (isScrolledIntoView('.contact-section')) {
            $('.contact-section .section-main-title').addClass('animate-line');
            $('.contact-section .section-main-title').addClass('animate-line');
        }

        if (isScrolledIntoView('.line-red')) {
            $('footer .line-red').addClass('red-line-animate');
        }

        if (isScrolledIntoView('.red-bg-section')) {
            $('.red-bg-section').addClass('footer-line-animate');
        }
    })
})()
</script>
<script type="text/javascript">
    $(".allownumericwithoutdecimal").on("keypress keyup blur",function (event) {    
            //$(this).val($(this).val().replace(/[^\d].+/, ""));
            if (event.which != 8 && event.which != 0 && (event.which < 48 || event.which > 57)) {
                event.preventDefault();
            }
        });
</script>