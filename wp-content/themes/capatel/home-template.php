<?php
/**
 * Template Name: Homepage
 *
 * @package WordPress
 * @subpackage Capatel
 * @since Capatel 1.0
 */

get_header();
$pageID = get_the_ID();
?>
<section class="slider-banner">
                <div class="owl-carousel owl-theme" id="banner-slider">
                   <?php _e(GetBannerSliders());?> 
                </div>
        </section>
         <section  id=firstSection class="product-section section" data-aos="fade-up"data-aos-anchor-placement="top-bottom">
            <div class="row">
                <h5 class="product-title-small">Our Businesses</h5>
                <h2 class="section-main-title text-center animated slideInUp delay-1s">Fashioning Growth</h2>
            </div>
           
            <div class="row product-section-blog">
                <div class="main-product-blog">
                	<?php
                	_e(GetProductsByCategory());
                	?>
                </div>
            </div>
        </section>
        <!-- video section start -->
        <section id=secondSection class="video-section section" data-aos="fade-up"data-aos-anchor-placement="top-bottom">
            <div class="container">
                <div class="overlay-video-text">
                    <?php the_field('video_desc',$pageID);?>
                    <a href="#" class="play-icon" data-toggle="modal" data-target="#exampleModalCenter"><img src="<?php echo get_template_directory_uri()?>/img/play_icon.png"
                            alt=""> </a>
                    <?php the_field('video_desc2',$pageID);?>
                </div>
            </div>
        </section>
        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered " role="document">
                <div class="modal-content video-modal">
                    <div class="modal-body">
                        <iframe width="100%" height="350" src="<?php the_field('video_url',$pageID);?>"
                            frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
        <!-- video section end -->
        <!-- certification section start -->
        <section class="certification-section" data-aos="fade-up"data-aos-anchor-placement="top-bottom">
            <div class="container-fluid">
                <div class="row flex-nowrap flex-md-wrap flex-sm-wrap justify-content-between">
                    <div class="col-5 col-lg-5 col-md-12 col-sm-12 wow fadeInLeft">
                        <div class="sedex-logo pattarn-bg">
                        	<?php 
							$image = get_field('certification_image',$pageID);
							if( !empty($image) ): ?>
								<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
							<?php endif; ?>
                        </div>
                    </div>
                    <div class="col-7 col-lg-7 col-md-12 col-sm-12 justify-content-end wow fadeInRight">
                        <div class="sedex-text">
                		<?php the_field('certification_description',$pageID);?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- certification section end -->

        <!-- redline section start -->
        <section class="red-line-section" data-aos="fade-up"data-aos-anchor-placement="top-bottom"> </section>
        <!-- redline section end -->

        <!-- testimonial section start -->
        <section class="testimonial-section" data-aos="fade-up"data-aos-anchor-placement="top-bottom">
            <div class="container">
                <div class="testimonial-main">
                    <h2 class="section-main-title text-center wow fadeInRight">Management</h2>
                    <div class="row row-centered">
                        <div class="col-12 mx-auto text-center">
                            <div class="testimonial-main-section  wow fadeInLeft">
                                <div class="single-section-main">
                                    <div class="owl-carousel" id="quote_carousel">
                                     <?php _e(GetManagementTestimonials());?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- testimonial section end -->
        <section class="client-section">
            <div class="container">
                <h2 class="section-main-title text-center  wow fadeInRight">Our Fabulous Clientele</h2>
            </div>

            <div class="owl-carousel owl-theme padding-40" id="client_logo">
                <?php _e(GetClientLogos());?>
            </div>
        </section>
<?php
get_footer();
?>
<script>
var owl = $('#banner-slider');
owl.owlCarousel({
        items: 1,
        loop: true,
        autoplay: true,
        responsiveClass: true,
        nav: false,
        slideTransition: 'linear',
        autoplaySpeed: 1000,
        dots: false,
        mouseDrag: false,
        animateOut: 'fadeOut',
        touchDrag: true,
        autoPlay: 1000,
        slideSpeed : 5000,
      smartSpeed:2000,
});

owl.on('changed.owl.carousel', function(event) {
  var item = event.item.index - 2; 
//   console.log(event.item.index)   
  $('.slam-text').removeClass('slide-demo');
    $('.owl-item').not('.cloned').eq(item).find('.slam-text').addClass('slide-demo');
});
</script>

<script>
    var owl = $('#quote_carousel');
        owl.owlCarousel({
            items:1,
            loop:true,
            nav:true,
            autoplay:true,
            autoplayTimeout:3000,
            autoplayHoverPause:true,
            slideSpeed : 2000,
            smartSpeed:2000,
        });
</script>
<script>
    jQuery('#client_logo').owlCarousel({
        items: 6,
        loop: true,
        margin: 10,
        autoplay: 2000,
        responsiveClass: true,
        autoplayTimeout: 50000,
        slideTransition: 'linear',
        animateOut: 'slideOutDown',
        animateIn: 'flipInX',
        nav: false,
        autoplayTimeout: 5000,
        slideSpeed : 2000,
        smartSpeed:2000,
        dots: true,
        responsive: {
            0: {
                items: 1,
                nav: false
            },
            600: {
                items: 3,
                nav: false
            },
            1000: {
                items: 6,
                nav: false,
                loop: false
            }
        }
    })

</script>
</script>
<!--Script for fade in about images end-->
<script>       
    (function() {
        $(window).scroll(function() {
            
            if(isScrolledIntoView('#firstSection')) {
                $('#firstSection .section-main-title').addClass('animate-line');
                $('#firstSection .section-main-title').addClass('animate-line');
            }
            if(isScrolledIntoView('.testimonial-section')) {
                $('.testimonial-section .section-main-title').addClass('animate-line');
                $('.testimonial-section .section-main-title').addClass('animate-line');
            }

             if(isScrolledIntoView('.client-section')) {
                $('.client-section .section-main-title').addClass('animate-line');
                $('.client-section .section-main-title').addClass('animate-line');
            }

            if(isScrolledIntoView('#secondSection')) {
                $('#secondSection .section-main-title').addClass('animate-line');
                $('#secondSection .section-main-title').addClass('animate-line');
                $('#secondSection').addClass('animate-line-vertical');
            }
            if(isScrolledIntoView('.certification-section')) {
                $('.certification-section .sedex-logo').addClass('animate-line-sedex-logo');
                $('.certification-section .sedex-text .sub-title').addClass('animate-line-sedex');
            }
            if(isScrolledIntoView('.line-red')) {
                $('footer .line-red').addClass('red-line-animate');
            }

             if(isScrolledIntoView('.red-bg-section')) {
                $('.red-bg-section').addClass('footer-line-animate');
            }
        })
    })()
</script>