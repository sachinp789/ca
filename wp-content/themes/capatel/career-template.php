<?php
/**
 * Template Name: Careers
 *
 * @package WordPress
 * @subpackage Capatel
 * @since Capatel 1.0
 */
get_header();
$pageID = get_the_ID();
$image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()), 'full' );
?>

        <section class="inner-banner-section">
            <img src="<?php echo $image; ?>">
            <div class="banner">
                <h2 class="section-main-title text-center">
                    <?php echo get_field('banner_title'); ?>
                </h2>
            </div>
            <div class="banner-line"></div>
        </section>

        <section class="career-section bg-body">
            <div class="container">
                <div class="row">
                    <div class="col-12 wow fadeInLeft mx-auto">
                        <p class="career-text">
                            <?php 
                                echo get_field('activity_tagline');
                            ?>
                        </p>
                    </div>
                    <div class="activity-section">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-8 col-sm-12">
                                    <div class="activity-section-img">
                                        <?php $image = get_field('image_1'); ?>
                                        <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-12">
                                        <div class="activity-section-img">
                                            <?php $image = get_field('image_2'); ?>
                                            <img class="m-b-20" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                                        </div>
                                        <div class="activity-section-img">
                                            <?php $image = get_field('image_3'); ?>
                                            <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="apply-form-modal">
            <div class="modal fade" id="apply-form" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <!-- Modal Header -->
                        <div class="modal-header">

                            <h4 class="modal-title" id="myModalLabel">
                                Apply for Position
                               
                            </h4>
                            <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">&times;</span>
                                <span class="sr-only">Close</span>
                            </button>
                        </div>

                        <!-- Modal Body -->
                        <div class="modal-body clearfix">
                            <div class="mail-error"></div>
                            <div class="mail-success"></div>
                            <?php echo do_shortcode('[contact-form-7 id="213" title="Careers Form"]'); ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="career-opening-section bg-body">
            <div class="container">
                <div class="row padding-15">
                   <div class="opening-sub-title">
                   <div class="col-md-8 p-l-0">
                    <h2 class="section-main-title text-uppercase">
                        <?php 
                            echo get_field('opening_title');
                        ?>
                    </h2>
                    </div>
                    <div class="col-md-4 p-r-0">
                    <select name="" id="location_filter" class="form-control">
                        <option value="All">Sort By Location</option>
                        <option value="Ahmedabad">Ahmedabad</option>
                        <option value="Bangaluru">Bangaluru</option>
                    </select>
                    </div>
                    </div>
                    <div class="panel-group" id="accordion"  role="tablist" aria-multiselectable="false">
                        <?php

                            $args = array(
                                    'post_type' => 'wporg_positions',
                                );
                            $category_posts = new WP_Query($args);

                            if($category_posts->have_posts()) : 
                                while($category_posts->have_posts()) : 
                                    $category_posts->the_post();
                            ?>

                                  <div class="panel panel-default">
                            <div class="panel-heading" id="headingOne">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-target="#collapse-<?php echo get_the_ID(); ?>" aria-expanded="true" aria-controls="collapse-<?php echo get_the_ID(); ?>">
                                        <i class="more-less fa fa-plus"></i>
                                        <?php the_title();  ?>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse-<?php echo get_the_ID(); ?>" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="panel-body">
                                    <a class="btn-red apply_btn" data-button-label="Apply Now" data-toggle="modal" data-target="#apply-form">
                                        <span class="button__label form_target" data-opening="<?php the_title(); ?>">Apply Now</span>
                                    </a>
                                    <h5 class="job-position-title">Department : 
                                        <span>
                                            <?php 
                                                $dept = get_field('department');
                                                echo $dept->post_title;
                                             ?>
                                        </span>
                                    </h5>
                                    <h5 class="job-position-title">Experience :
                                        <span><?php echo get_field('experience'); ?></span>
                                    </h5>
                                    <h5 class="job-position-title">Job Description : </h5>
                                    <?php
                                        echo get_the_content();
                                     ?>
                                </div>
                            </div>
                        </div>
                            <?php
                                endwhile;
                            else: 
                            ?>
                                Sorry, currently there are no open positions.
                            <?php
                            endif;
                            ?>    
                    </div> 
                    
                    <!-- panel-group -->
                </div>
            </div>
        </section>
        
  <?php
get_footer();
?>
  
<script>
        AOS.init({
            easing: 'ease-out-back',
            duration: 3000
        });
    </script>
    <!-- <script>
        new WOW().init();
        //    
        //    var path = document.querySelector('.path-vert');
        //var length = path.getTotalLength();

    </script> -->
    <script>
            function isScrolledIntoView(elem) {
                var docViewTop = $(window).scrollTop();
                var docViewBottom = docViewTop + $(window).height();
    
                var elemTop = $(elem).offset().top;
                var elemBottom = elemTop + $(elem).height();
                return ((elemTop <= docViewBottom) && (elemTop >= docViewTop));
            }
    
            (function () {
                $(window).scroll(function () {
                    if (isScrolledIntoView('.inner-banner-section')) {
                    $('.inner-banner-section .section-main-title').addClass('animate-line');
                    $('.inner-banner-section .section-main-title').addClass('animate-line');
                }
                    if (isScrolledIntoView('.career-section')) {
                        $('.career-section .section-main-title').addClass('animate-line');
                        $('.career-section .section-main-title').addClass('animate-line');
                    }
                    if (isScrolledIntoView('.career-opening-section')) {
                        $('.career-opening-section .section-main-title').addClass('animate-line');
                        $('.career-opening-section .section-main-title').addClass('animate-line');
                    }
    
                    if (isScrolledIntoView('.line-red')) {
                        $('footer .line-red').addClass('red-line-animate');
                    }
    
                    if (isScrolledIntoView('.red-bg-section')) {
                        $('.red-bg-section').addClass('footer-line-animate');
                    }
                })
            })()
        </script>
    
        <script>
            // navbar js
            $(document).ready(function () {
                $(window).scroll(function () {
                    var scroll = $(window).scrollTop();
    
                    if (scroll > 350) {
                        $(".navbar").addClass('nav-white');
                    }
                    else {
                        $(".navbar").removeClass('nav-white');
                    }
                })
            })</script>

            <script>
                function toggleIcon(e) {
                    $(e.target).prev('.panel-heading').find(".more-less").toggleClass('fa-minus');
                }
                $('.panel-group').on('hidden.bs.collapse', toggleIcon);
                $('.panel-group').on('shown.bs.collapse', toggleIcon);
            </script>


            <!-- SCRIPT FOR INPUT PREFILLING AND FILTER -->
            <script>
                $('#wpcf7-f213-o1').find('form').addClass('contact-form')
                $('.apply_btn').on('click', function() {
                    $('#inputposition').attr('readonly',true)
                    $('#inputposition').val($(this).children().data('opening'))
                })
                $(document).ready(function() {
                    $('#location_filter').on('change', function() {
                        let location = $(this, 'option: selected').val()
                        $.ajax({
                            type : "post",
                            url : "<?php echo admin_url('admin-ajax.php'); ?>" ,
                            data : {action: "wp_get_positions_by_location", location : location},
                            success: function(data) {
                                $('#accordion').html(data)
                                $(".apply_btn").on("click", function() {
                                   $("#inputposition").val($(this).children().data("opening"))
                                })
                            }
                        })
                    })
                })
            </script>