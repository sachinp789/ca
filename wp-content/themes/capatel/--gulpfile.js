'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var minifyCSS = require('gulp-clean-css')
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var changed = require('gulp-changed');
var concat = require('gulp-concat');
var minifyjs = require('gulp-js-minify');
var order = require('gulp-order');

//////////////////
//// -- CSS
//////////////////

var SCSS_BOOTSTRAP_SRC = 'scss/bootstrap.scss';
var SCSS_BOOTSTRAP_DEST = './css';

var SCSS_CUSTOM_SRC = 'scss/custom.scss';
var SCSS_CUSTOM_DEST = './css';

var SCSS_RESPONSIVE_SRC = 'scss/responsive.scss';
var SCSS_RESPONSIVE_DEST = './css';



//var JS_SRC = 'assets/js/src/**/*.js';
//var JS_DEST = 'assets/js';


//compile js
//gulp.task('compile_js', function(){
//    gulp.src(JS_SRC)
//        .pipe(concat('main.js'))
//        .pipe(minifyjs())
//        .pipe(gulp.dest(JS_DEST));
//});

//compile scss
gulp.task('compile_bootstrap_scss', function () {
    gulp.src(SCSS_BOOTSTRAP_SRC)
        .pipe(sass().on('error', sass.logError))
//        .pipe(minifyCSS())
        .pipe(rename({suffix:'.min'}))
        .pipe(changed(SCSS_BOOTSTRAP_DEST))
        .pipe(gulp.dest(SCSS_BOOTSTRAP_DEST))
});


//compile scss
gulp.task('compile_custom_scss', function () {
    gulp.src(SCSS_CUSTOM_SRC)
        .pipe(sass().on('error', sass.logError))
        .pipe(minifyCSS())
        .pipe(rename({suffix:'.min'}))
        .pipe(changed(SCSS_CUSTOM_DEST))
        .pipe(gulp.dest(SCSS_CUSTOM_DEST))
});

//compile scss
gulp.task('compile_responsive_scss', function () {
    gulp.src(SCSS_RESPONSIVE_SRC)
        .pipe(sass().on('error', sass.logError))
        .pipe(minifyCSS())
        .pipe(rename({suffix:'.min'}))
        .pipe(changed(SCSS_RESPONSIVE_DEST))
        .pipe(gulp.dest(SCSS_RESPONSIVE_DEST))
});


//detect changes in js
//gulp.task('watch_js', function () {
//    gulp.watch(JS_SRC, ['compile_js']);
//});


//detect changes in scss
gulp.task('watch_bootstrap_scss', function () {
    gulp.watch(SCSS_BOOTSTRAP_SRC, ['compile_bootstrap_scss']);
});

//detect changes in scss
gulp.task('watch_custom_scss', function () {
    gulp.watch(SCSS_CUSTOM_SRC, ['compile_custom_scss']);
});

//detect changes in scss
gulp.task('watch_responsive_scss', function () {
    gulp.watch(SCSS_RESPONSIVE_SRC, ['compile_responsive_scss']);
});


//Run tasks
gulp.task('default', ['watch_bootstrap_scss', 'watch_custom_scss', 'watch_responsive_scss']);