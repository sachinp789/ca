<?php
/**
 * Template Name: Product
 *
 * @package WordPress
 * @subpackage Capatel
 * @since Capatel 1.0
 */
get_header();
$pageID = get_the_ID();
$content_post = get_post($pageID);
//$image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()), 'full' );
?>
<div class="container">
    <div class="col-12 mx-auto text-center">
    </div>
</div>
<section class="product-banner">
            <div class="row flex align-items-center mx-0">
                <?php $images = acf_photo_gallery('pgallery',$pageID);
                if( $images ): ?>
                    <?php foreach( $images as $image ): ?>  
                    <div class="product-item">
                        <img src="<?php echo $image['full_image_url']; ?>" alt="<?php echo $image['title']; ?>" />
                    </div>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>
            <div class="container">
                <div class="row">
                    <div class="overlay-title">
                        <h1><?php the_field('slogan',$pageID);?></h1>
                    </div>
                </div>
            </div>
        </section>
         <section class="red-line-section" data-aos="fade-down" data-aos-anchor-placement="top-bottom"> </section>

        <section>
            <div class="main-section-section" style="flex-wrap: wrap;">
                <?php 
                    $args = array(
                        'post_type' => 'post',
                        'posts_per_page' => 5,
                        'post_status' => 'publish'
                    );

                    $query = new WP_Query($args);
                    if( $query->have_posts() ):
                    $counter = 1;
                    while( $query->have_posts() ): $query->the_post();
                ?>
                  <div class="product-section-item left-animate <?php if($counter == 5) {echo "last-section-item"; } ?> " data-aos="fade-up" data-aos-anchor-placement="top-bottom">
                    <div class="product-items"><?php echo get_the_post_thumbnail(); ?></div>
                    <div class="product-items border-red">
                        <div class="product-details">
                            <h2 class="product-title"><?php echo get_the_title(); ?></h2>
                            <p class="detail-item">
                                <?php 
                                   echo mb_strimwidth(get_the_content(), 0, 140, '...');
                                ?>
                            </p>
                            <a href="<?php echo get_the_permalink(); ?>" class="main-btn">Read More</a>
                        </div>
                    </div>
                    <div class="line">
                        <div class="line-inner"></div>
                        <div class="circle-inner"></div>
                    </div>
                </div>
                <?php
                    $counter++;
                    endwhile;
                    endif;
                    wp_reset_postdata();
                    
                ?>
                <div class="border-bodt">
                    <div class="product-items last-product-item"><img src="<?php _e(get_template_directory_uri())?>/img/product-6.jpg" alt="" class="contact-product-img"></div>
                    <a href="<?php _e(site_url())?>/contact-us" class="main-btn">Contact us</a>
                </div>
            </div>
        </section>


        <style>
            .main-section-section .product-section-item:nth-child(n+3) .product-items {
                order: 1;
            }
            .main-section-section .product-section-item:nth-child(n+3) .product-items.border-red {
                order: 0;
            }
            .main-section-section .product-section-item .line {
                display: flex;
                align-items: center;
                position: absolute;
                top: 80%;
                left: 50%;
                transform: translateX(-50%);
            }
            .main-section-section .product-section-item:nth-child(n+3) .line .circle-inner {
                order: 0;
            }

             .main-section-section .product-section-item:nth-child(n+3) .line .line-inner {
                order: 1;
            }
            .main-section-section .product-section-item .line .circle-inner {
                width: 15px;
                height: 15px;
                background: #c12728;
                border-radius: 100px;
            }
            .main-section-section .product-section-item .line .line-inner {
                width: 100px;
                border-top:2px solid #c12728;
            }
            .main-section-section .last-section-item .product-items
            {
                order:1 !important;
                border-bottom:none;
            }
            .main-section-section .last-section-item .line .circle-inner {
                order: 1 !important;
             }
            .main-section-section .last-section-item .line .line-inner {
                order: 0 !important;
            }
            </style>
<?php
get_footer();
?>
<script>
    function showml(divId, inhtmText) {
        var x = document.getElementById(divId).style.display;

        if (x == "block") {
            document.getElementById(divId).style.display = "none";
            document.getElementById(inhtmText).innerHTML = "Show More";
        }
        if (x == "none") {
            document.getElementById(divId).style.display = "block";
            document.getElementById(inhtmText).innerHTML = "Show Less";
        }
    }
</script>   
<script type="text/javascript">
(function () {
    $(window).scroll(function () {

        if(isScrolledIntoView('.product-section-item')) {
            $('.product-section-item').addClass('animate-pline');
            
            // $('.inner-banner-section .section-main-title').addClass('animate-line');
        }
        // if(isScrolledIntoView('.left-animate')) {
        //     $('.left-animate').addClass('animate-pline');
        // }

        
        // if(isScrolledIntoView('.team-section')) {
        //     $('.team-section .section-main-title').addClass('animate-line');
        //     $('.team-section .section-main-title').addClass('animate-line');
        // }

        if (isScrolledIntoView('.line-red')) {
            $('footer .line-red').addClass('red-line-animate');
        }

        if (isScrolledIntoView('.red-bg-section')) {
            $('.red-bg-section').addClass('footer-line-animate');
        }
    })
})()
</script>