<?php
/**
 * A unique identifier is defined to store the options in the database and reference them from the theme.
 */
function optionsframework_option_name() {
	// Change this to use your theme slug
	return 'options-framework-theme';
}

function optionsframework_options() {

	// Test data
	$test_array = array(
		'one' => __( 'One', 'theme-searchnative' ),
		'two' => __( 'Two', 'theme-searchnative' ),
		'three' => __( 'Three', 'theme-searchnative' ),
		'four' => __( 'Four', 'theme-searchnative' ),
		'five' => __( 'Five', 'theme-searchnative' )
	);

	// Multicheck Array
	$multicheck_array = array(
		'slider' => __( 'Slider', 'theme-searchnative' ),
		'testimonials' => __( 'Testimonials', 'theme-searchnative' ),
		'googlemap' => __( 'Google Map', 'theme-searchnative' ),
		'logos' => __( 'Our Partners', 'theme-searchnative' )
		/*'portfolio' => __( 'Portfolio', 'theme-searchnative' ),
		'latest_blog' => __( 'Latest Blog', 'theme-searchnative' )*/
	);

	// Multicheck Defaults
	$multicheck_defaults = array(
		'one' => '1',
		'four' => '1'
	);

	// Background Defaults
	$background_defaults = array(
		'color' => '',
		'image' => '',
		'repeat' => 'repeat',
		'position' => 'top center',
		'attachment'=>'scroll' );

	// Typography Defaults
	$typography_defaults = array(
		'size' => '15px',
		'face' => 'georgia',
		'style' => 'bold',
		'color' => '#bada55' );

	// Typography Options
	$typography_options = array(
		'sizes' => array( '6','12','14','16','20' ),
		'faces' => array( 'Helvetica Neue' => 'Helvetica Neue','Arial' => 'Arial' ),
		'styles' => array( 'normal' => 'Normal','bold' => 'Bold' ),
		'color' => false
	);

	// Pull all the categories into an array
	$options_categories = array();
	$options_categories_obj = get_categories();
	foreach ($options_categories_obj as $category) {
		$options_categories[$category->cat_ID] = $category->cat_name;
	}

	// Pull all tags into an array
	$options_tags = array();
	$options_tags_obj = get_tags();
	foreach ( $options_tags_obj as $tag ) {
		$options_tags[$tag->term_id] = $tag->name;
	}


	// Pull all the pages into an array
	$options_pages = array();
	$options_pages_obj = get_pages( 'sort_column=post_parent,menu_order' );
	$options_pages[''] = 'Select a page:';
	foreach ($options_pages_obj as $page) {
		$options_pages[$page->ID] = $page->post_title;
	}

	// If using image radio buttons, define a directory path
	$imagepath =  get_template_directory_uri() . '/assets/images/';

	$options = array();

	$options[] = array(
		'name' => __( 'General Settings', 'theme-searchnative' ),
		'type' => 'heading'
	);
	/*$options[] = array(
		'name' => __( 'Favicon', 'theme-searchnative' ),
		'desc' => __( '.ico 32x32 pixels', 'theme-searchnative' ),
		'id' => 'favicon_uploader',
		'type' => 'upload'
	);*/
	$options[] = array(
		'name' => __( 'Logo', 'theme-searchnative' ),
		'desc' => __( 'This creates a full size uploader that previews the image.', 'theme-searchnative' ),
		'id' => 'logo',
		'type' => 'upload'
	);
	
	

	$options[] = array(
		'name' => __( 'Slogan', 'theme-searchnative' ),
		'desc' => __( 'Have any questions?', 'theme-searchnative' ),
		'id' => 'slogan',
		'std' => '',		
		'type' => 'text'
	);	

	$options[] = array(
		'name' => __( 'Contact', 'theme-searchnative' ),
		'desc' => __( 'Contact number', 'theme-searchnative' ),
		'id' => 'phone_number',
		'placeholder' => 'Phone number',
		'type' => 'text'
	);
	
	$options[] = array(
		'name' => __( 'Email', 'theme-searchnative' ),
		'desc' => __( 'Email address', 'theme-searchnative' ),
		'id' => 'email',
		'placeholder' => 'Email address',
		'type' => 'text'
	);

	$options[] = array(
		'name' => __( 'Compney Address', 'theme-searchnative' ),
		'desc' => __( 'Compney Address', 'theme-searchnative' ),
		'id' => 'compney_address',		
		'type' => 'textarea'
	);

	$options[] = array(
		'name' => __( 'Google Maps API Key', 'theme-searchnative' ),
		'desc' => __( '<span class="description regular-text">If you are generating a large volume of queries to the Google Maps API please <a target="_blank" href="https://developers.google.com/maps/documentation/javascript/get-api-key#get-an-api-key">Get an API key</a></span>', 'theme-searchnative' ),
		'id' => 'google_map_api',
		'std' => '',
		'type' => 'text'
	);

	$options[] = array(
		'name' => __( 'Latitude', 'theme-searchnative' ),
		'desc' => __( '', 'theme-searchnative' ),
		'id' => 'latitude',
		'std' => '',
		'type' => 'text'
	);

	$options[] = array(
		'name' => __( 'Longitude', 'theme-searchnative' ),
		'desc' => __( '', 'theme-searchnative' ),
		'id' => 'longitude',
		'std' => '',
		'type' => 'text'
	);


	$options[] = array(
		'name' => __( 'Google Analytics', 'theme-searchnative' ),
		'desc' => __( 'Paste your Google Analytics code here.', 'theme-searchnative' ),
		'id' => 'google_analytics',		
		'type' => 'textarea'
	);

	/*$options[] = array(
		'name' => __( 'Input Select Small', 'theme-searchnative' ),
		'desc' => __( 'Small Select Box.', 'theme-searchnative' ),
		'id' => 'example_select',
		'std' => 'three',
		'type' => 'select',
		'class' => 'mini', //mini, tiny, small
		'options' => $test_array
	);

	$options[] = array(
		'name' => __( 'Input Select Wide', 'theme-searchnative' ),
		'desc' => __( 'A wider select box.', 'theme-searchnative' ),
		'id' => 'example_select_wide',
		'std' => 'two',
		'type' => 'select',
		'options' => $test_array
	);

	if ( $options_categories ) {
		$options[] = array(
			'name' => __( 'Select a Category', 'theme-searchnative' ),
			'desc' => __( 'Passed an array of categories with cat_ID and cat_name', 'theme-searchnative' ),
			'id' => 'example_select_categories',
			'type' => 'select',
			'options' => $options_categories
		);
	}

	if ( $options_tags ) {
		$options[] = array(
			'name' => __( 'Select a Tag', 'options_check' ),
			'desc' => __( 'Passed an array of tags with term_id and term_name', 'options_check' ),
			'id' => 'example_select_tags',
			'type' => 'select',
			'options' => $options_tags
		);
	}

	$options[] = array(
		'name' => __( 'Select a Page', 'theme-searchnative' ),
		'desc' => __( 'Passed an pages with ID and post_title', 'theme-searchnative' ),
		'id' => 'example_select_pages',
		'type' => 'select',
		'options' => $options_pages
	);

	$options[] = array(
		'name' => __( 'Input Radio (one)', 'theme-searchnative' ),
		'desc' => __( 'Radio select with default options "one".', 'theme-searchnative' ),
		'id' => 'example_radio',
		'std' => 'one',
		'type' => 'radio',
		'options' => $test_array
	);

	$options[] = array(
		'name' => __( 'Example Info', 'theme-searchnative' ),
		'desc' => __( 'This is just some example information you can put in the panel.', 'theme-searchnative' ),
		'type' => 'info'
	);

	$options[] = array(
		'name' => __( 'Input Checkbox', 'theme-searchnative' ),
		'desc' => __( 'Example checkbox, defaults to true.', 'theme-searchnative' ),
		'id' => 'example_checkbox',
		'std' => '1',
		'type' => 'checkbox'
	);*/

	/*$options[] = array(
		'name' => __( 'Advanced Settings', 'theme-searchnative' ),
		'type' => 'heading'
	);

	

	
	
	

	$options[] = array(
		'name' => __( 'Post Type | Enable', 'theme-searchnative' ),
		'desc' => __( 'If you do not want to use any of these Types, you can enable it', 'theme-searchnative' ),
		'id' => 'theme_functions',
		'std' => $multicheck_defaults, // These items get checked by default
		'type' => 'multicheck',
		'options' => $multicheck_array
	);*/



	
	/**
	 * For $settings options see:
	 * http://codex.wordpress.org/Function_Reference/wp_editor
	 *
	 * 'media_buttons' are not supported as there is no post to attach items to
	 * 'textarea_name' is set by the 'id' you choose
	 */

	$wp_editor_settings = array(
		'wpautop' => true, // Default
		'textarea_rows' => 5,
		'tinymce' => array( 'plugins' => 'wordpress,wplink' )
	);

	$options[] = array(
		'name' => __( 'Footer Text', 'theme-searchnative' ),
		'desc' => sprintf( __( 'Copyright Text', 'theme-searchnative' )),
		'id' => 'footer_text',
		'type' => 'editor',
		'settings' => $wp_editor_settings
	);

	/*$options[] = array(
		'name' => __( 'Social Media links', 'theme-searchnative' ),
		'desc' => __( 'Click here for social media', 'theme-searchnative' ),
		'id' => 'example_showhidden',
		'type' => 'checkbox'
	);*/
		$options[] = array(
			'name' => __( 'Social link Settings', 'theme-searchnative' ),
			'icon' => ' el-icon-credit-card',
			'type' => 'heading'
		);

		$options[] =  array(
			'name' => __( 'Facebook Url', 'theme-searchnative' ),		    
		    'type' => 'text',	
		    'id' => 'facebook_url',
		    'default' => '',
		);
		
		$options[] =  array(
			'name' => __( 'Twitter Url', 'theme-searchnative' ),
		    'id' => 'twitter_url',
		    'type' => 'text',  
		    'default' => 'https://twitter.com/',
		);
        $options[] =  array(
            'id' => 'linkedin_url',
            'type' => 'text',
            'name' => __('Linkedin Url', 'theme-searchnative'),              
            'default' => 'https://www.linkedin.com/',
        );
        $options[] =  array(
            'id' => 'instagam_url',
            'type' => 'text',
            'name' => __('Instagram Url', 'theme-searchnative'),              
            'default' => 'https://www.instagram.com/',
        );
	return $options;
}