<?php
/**
 * Template Name: About
 *
 * @package WordPress
 * @subpackage Capatel
 * @since Capatel 1.0
 */
get_header();
$pageID = get_the_ID();
$content_post = get_post($pageID);
$image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()), 'full' );
?>
<div class="container">
    <div class="col-12 mx-auto text-center">
    </div>
</div>
<section class="inner-banner-section">
    <img src="<?php echo $image?>">
    <div class="banner">
        <div class="section-main-title">
            <?php _e(get_the_Title())?>
        </div>
    </div>
    <div class="banner-line"></div>
</section>
<section class="about-section bg-body">
    <div class="container about-body">
        <div class="row">
            <div class="col-12 wow fadeInLeft mx-auto">
                <p class="regular-text text-center m-b-40"><?php _e($content_post->post_content);?></p>
                <div class="row d-flex">
                    <div class="col-6 col-lg-6 col-xl-6 col-sm-12 col-md-12">
                    	<?php 
						$image = get_field('about_image',$pageID);
						if( !empty($image) ): ?>
							<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="about-img w-100 img-fluid" />
						<?php endif; ?>
                    </div>
                    <div class="col-6 col-lg-6 col-xl-6 col-sm-12 col-md-12">
                    	<?php the_field('about_description',$pageID);?>
                    </div>
                </div>

                <div class="row">
                    <div class="col-6">
                        <div class="mission-vision mission-border">
                            <h5 class="sub-title">Vision</h5>
                            <p class="light-text"><?php the_field('vision',$pageID);?></p>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="mission-vision">
                            <h5 class="sub-title">Mission</h5>
                            <p class="light-text"><?php the_field('mission',$pageID);?></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="about-counter-section" data-aos="fade-up" data-aos-anchor-placement="top-bottom">
    <div class="container">
        <h2 class="section-main-title text-center wow fadeInRight">Fashioning Sustainability</h2>
        <div class="row justify-content-between text-center">
          <?php the_field('fashion_content',$pageID);?>
        </div>
    </div>
</section>
<section class="team-section bg-body" data-aos="fade-up" data-aos-anchor-placement="top-bottom">
    <div class="container">
        <h2 class="section-main-title text-center wow fadeInRight">Our Team</h2>
        <div class="row team-members leader-member">
            <?php _e(GetLeaderTeam('leader'));?>
        </div>
        <div class="row team-members junior-member">
        	<?php _e(GetJuniorTeam('junior'));?>
        </div>
    </div>
</section>
<?php
get_footer();
?>
<script type="text/javascript">
(function () {
    $(window).scroll(function () {
        if (isScrolledIntoView('.inner-banner-section')) {
            $('.inner-banner-section .section-main-title').addClass('animate-line');
            $('.inner-banner-section .section-main-title').addClass('animate-line');
        }
        if (isScrolledIntoView('.about-counter-section')) {
            $('.about-counter-section .section-main-title').addClass('animate-line');
            $('.about-counter-section .section-main-title').addClass('animate-line');
        }
        
        if (isScrolledIntoView('.team-section')) {
            $('.team-section .section-main-title').addClass('animate-line');
            $('.team-section .section-main-title').addClass('animate-line');
        }

        if (isScrolledIntoView('.line-red')) {
            $('footer .line-red').addClass('red-line-animate');
        }

        if (isScrolledIntoView('.red-bg-section')) {
            $('.red-bg-section').addClass('footer-line-animate');
        }
    })
})()
</script>