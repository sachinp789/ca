</main>
<footer>
	<div class="container-fluid">
	    <div class="row wow fadeInLeft">
	        <div class="col-4 col-lg-4 col-md-6 col-sm-6 red-bg-section">
	           <?php _e(GetPostsByCategory());?>
	        </div>
	        <div class="col-2 col-lg-2 col-md-6 col-sm-6">
	            <div class="footer-pd">
	                <h4 class="footer-title">Quick Links</h4>
	                <div class="line-red"></div>
	                <?php wp_nav_menu( array( 'container_class' => '','items_wrap' =>'<ul class="product-list">%3$s</ul>','theme_location' => 'social' ) ); ?>
	                <ul class="social-list">
	                    <li><a href="<?php echo of_get_option('facebook_url'); ?>"><i class="fa fa-facebook"></i></a></li>
	                    <li><a href="<?php echo of_get_option('twitter_url'); ?>"><i class="fa fa-twitter"></i></a></li>
	                    <li><a href="<?php echo of_get_option('instagam_url'); ?>"><i class="fa fa-instagram"></i></a></li>
	                    <li><a href="<?php echo of_get_option('linkedin_url'); ?>"><i class="fa fa-linkedin"></i></a></li>
	                </ul>
	            </div>
	        </div>
	        <div class="col-5 col-lg-5 col-md-12 col-sm-12">
	            <div class="footer-pd">
	                <div class="map-section">
	                    <iframe src="https://www.google.com/maps/embed?pb=!1m12!1m8!1m3!1d117489.76864697644!2d72.46510123151279!3d23.04009589086124!3m2!1i1024!2i768!4f13.1!2m1!1sHirabhai+Market%2C+Near+Diwan+Ballubhai+School%2C!5e0!3m2!1sen!2sin!4v1539093140083" width="100%" height="120" frameborder="0" style="border:0" allowfullscreen=""></iframe>
	                </div>
	                <div class="footer-address d-flex justify-content-between">
	                    <div class="address">
	                       <?php dynamic_sidebar( 'sidebar-2' );?>
	                    </div>
	                    <div class="address">
	                       <?php dynamic_sidebar( 'sidebar-3' );?>
	                    </div>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>
	<?php $footertext = of_get_option('footer_text'); ?>
	<div class="copyright">
	    <p class="copyright"><?php echo $footertext;?></p>
	</div>
</footer>
<?php wp_footer();?>
<script src="<?php echo get_template_directory_uri()?>/js/jquery.min.js"></script>
<script src="<?php echo get_template_directory_uri()?>/js/bootstrap.min.js"></script>
<script src="<?php echo get_template_directory_uri()?>/js/owl.carousel.min.js"></script>
<script src="<?php echo get_template_directory_uri()?>/js/aos.js"></script>
<script>
    AOS.init({
        easing: 'ease-out-back',
        duration: 3000
    });
    // navbar js
    $(document).ready(function () {
        $(window).scroll(function () {
            var scroll = $(window).scrollTop();

            if (scroll > 50) {
                $(".navbar").addClass('nav-white');
            }
            else {
                $(".navbar").removeClass('nav-white');
            }
        })
    })
</script>
<script type="text/javascript">
function isScrolledIntoView(elem) {
    var docViewTop = $(window).scrollTop();
    var docViewBottom = docViewTop + $(window).height();

    var elemTop = $(elem).offset().top;
    var elemBottom = elemTop + $(elem).height();
    return ((elemTop <= docViewBottom) && (elemTop >= docViewTop));
}
</script> 