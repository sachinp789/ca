<?php
define( 'OPTIONS_FRAMEWORK_DIRECTORY', get_template_directory_uri() . '/theme-options/' );
require_once get_template_directory().'/theme-options/options-framework.php';

$optionsfile = locate_template( 'options.php' );
load_template( $optionsfile );

function snative_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/snative
	 * If you're building a theme based on Twenty Seventeen, use a find and replace
	 * to change 'snative' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'snative' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *	 
	 */
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'snative-featured-image', 2000, 1200, true );
	add_image_size( 'snative-thumbnail-avatar', 100, 100, true );

	// Set the default content width.
	$GLOBALS['content_width'] = 525;

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'top'    => __( 'Top Menu', 'snative' ),
		'social' => __( 'Social Links Menu', 'snative' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		'gallery',
		'audio',
	) );

	// Add theme support for Custom Logo.
	add_theme_support( 'custom-logo', array(
		'width'       => 250,
		'height'      => 250,
		'flex-width'  => true,
	) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, and column width.
 	 */
	//add_editor_style( array( 'assets/css/editor-style.css', snative_fonts_url() ) );

	// Define and register starter content to showcase the theme on new sites.
	$starter_content = array(
		'widgets' => array(
			// Place three core-defined widgets in the sidebar area.
			'sidebar-1' => array(
				'text_business_info',
				'search',
				'text_about',
			),

			// Add the core-defined business info widget to the footer 1 area.
			'sidebar-2' => array(
				'text_business_info',
			),

			// Put two core-defined widgets in the footer 2 area.
			'sidebar-3' => array(
				'text_about',
				'search',
			),
			'sidebar-4' => array(
				'text_business_info',
			),
		),

		// Specify the core-defined pages to create and add custom thumbnails to some of them.
		'posts' => array(
			'home',
			'about' => array(
				'thumbnail' => '{{image-sandwich}}',
			),
			'contact' => array(
				'thumbnail' => '{{image-espresso}}',
			),
			'blog' => array(
				'thumbnail' => '{{image-coffee}}',
			),
			'homepage-section' => array(
				'thumbnail' => '{{image-espresso}}',
			),
		),

		// Create the custom image attachments used as post thumbnails for pages.
		'attachments' => array(
			'image-espresso' => array(
				'post_title' => _x( 'Espresso', 'Theme starter content', 'snative' ),
				'file' => 'assets/images/espresso.jpg', // URL relative to the template directory.
			),
			'image-sandwich' => array(
				'post_title' => _x( 'Sandwich', 'Theme starter content', 'snative' ),
				'file' => 'assets/images/sandwich.jpg',
			),
			'image-coffee' => array(
				'post_title' => _x( 'Coffee', 'Theme starter content', 'snative' ),
				'file' => 'assets/images/coffee.jpg',
			),
		),

		// Default to a static front page and assign the front and posts pages.
		'options' => array(
			'show_on_front' => 'page',
			'page_on_front' => '{{home}}',
			'page_for_posts' => '{{blog}}',
		),

		// Set the front page section theme mods to the IDs of the core-registered pages.
		'theme_mods' => array(
			'panel_1' => '{{homepage-section}}',
			'panel_2' => '{{about}}',
			'panel_3' => '{{blog}}',
			'panel_4' => '{{contact}}',
		),

		// Set up nav menus for each of the two areas registered in the theme.
		'nav_menus' => array(
			// Assign a menu to the "top" location.
			'top' => array(
				'name' => __( 'Top Menu', 'snative' ),
				'items' => array(
					'link_home', // Note that the core "home" page is actually a link in case a static front page is not used.
					'page_about',
					'page_blog',
					'page_contact',
				),
			),

			// Assign a menu to the "social" location.
			'social' => array(
				'name' => __( 'Social Links Menu', 'snative' ),
				'items' => array(
					'link_yelp',
					'link_facebook',
					'link_twitter',
					'link_instagram',
					'link_email',
				),
			),
		),
	);
	
	$starter_content = apply_filters( 'snative_starter_content', $starter_content );

	add_theme_support( 'starter-content', $starter_content );
}
add_action( 'after_setup_theme', 'snative_setup' );

function snative_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Blog Sidebar', 'snative' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar on blog posts and archive pages.', 'snative' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer 1', 'snative' ),
		'id'            => 'sidebar-2',
		'description'   => __( 'Add widgets here to appear in your footer.', 'snative' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer 2', 'snative' ),
		'id'            => 'sidebar-3',
		'description'   => __( 'Add widgets here to appear in your footer.', 'snative' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer 3', 'snative' ),
		'id'            => 'sidebar-4',
		'description'   => __( 'Add widgets here to appear in your footer.', 'snative' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'snative_widgets_init' );

function capatel_enqueue_style()
{	
	// Css load
	wp_enqueue_style( 'capatel-owl.carousel.min', get_template_directory_uri().'/css/owl.carousel.min.css');
	wp_enqueue_style( 'capatel-bootstrap.min', get_template_directory_uri().'/css/bootstrap.min.css');
	wp_enqueue_style( 'capatel-animate', get_template_directory_uri().'/css/animate.css');
	wp_enqueue_style( 'capatel-font-awesome.min', get_template_directory_uri().'/css/font-awesome.min.css');
	wp_enqueue_style( 'capatel-custom.min', get_template_directory_uri().'/css/custom.min.css');
	wp_enqueue_style( 'capatel-responsive.min', get_template_directory_uri().'/css/responsive.min.css');
	wp_enqueue_style( 'capatel-aos', get_template_directory_uri().'/css/aos.css'); 	
}
add_action( 'wp_enqueue_scripts', 'capatel_enqueue_style' );

add_action( 'optionsframework_custom_scripts', 'optionsframework_custom_scripts' );

function optionsframework_custom_scripts() { ?>

<script type="text/javascript">
jQuery(document).ready(function() {

	jQuery('#example_showhidden').click(function() {
  		jQuery('#section-example_text_hidden').fadeToggle(400);
	});

	if (jQuery('#example_showhidden:checked').val() !== undefined) {
		jQuery('#section-example_text_hidden').show();
	}

});
</script>
<?php
}

// Custom post testimonial register
function wporg_custom_post_type()
{
    register_post_type('wporg_product',
       array(
           'labels'      => array(
               'name'          => __('Testimonials'),
               'singular_name' => __('Testimonial'),
           ),
           'public'      => true,
           'has_archive' => true,
           'rewrite'     => array( 'slug' => 'management' ), // my custom slug
           'supports' => array('title', 'editor', 'thumbnail')
       )
    );
}
add_action('init', 'wporg_custom_post_type');

// Custom post client logo register
function wporg_custom_client_post_type()
{
    register_post_type('wporg_client',
       array(
           'labels'      => array(
               'name'          => __('Clients'),
               'singular_name' => __('Client'),
           ),
           'public'      => true,
           'has_archive' => true,
           'rewrite'     => array( 'slug' => 'client' ), // my custom slug
           'supports' => array('title', 'editor', 'thumbnail')
       )
    );
}
add_action('init', 'wporg_custom_client_post_type');

// Custom post slider register
function wporg_custom_slider_post_type()
{
    register_post_type('wporg_slider',
       array(
           'labels'      => array(
               'name'          => __('Banner Sliders'),
               'singular_name' => __('Banner Slider'),
           ),
           'public'      => true,
           'has_archive' => true,
           'rewrite'     => array( 'slug' => 'banner' ), // my custom slug
           'supports' => array('title', 'editor', 'thumbnail')
       )
    );
}
add_action('init', 'wporg_custom_slider_post_type');

// Custom post positions
function wporg_positions_post_type()
{
    register_post_type('wporg_positions',
       array(
           'labels'      => array(
               'name'          => __('Openings'),
               'singular_name' => __('Opening'),
           ),
           'public'      => true,
           'has_archive' => true,
           'rewrite'     => array( 'slug' => 'openings' ), // my custom slug
           'supports' => array('title', 'editor')
       )
    );
}
add_action('init', 'wporg_positions_post_type');

// Custom post our team
function wporg_team_post_type()
{
    register_post_type('wporg_team',
       array(
           'labels'      => array(
               'name'          => __('Our Team'),
               'singular_name' => __('Team Members'),
           ),
           'public'      => true,
           'has_archive' => true,
           'rewrite'     => array( 'slug' => 'team' ), // my custom slug
           'supports'    => array('title', 'editor', 'thumbnail'),
           'taxonomies'  => array( 'category' )
       )
    );
}
add_action('init', 'wporg_team_post_type');


// Custom post Department
function wporg_departments_post_type()
{
    register_post_type('wporg_departments',
       array(
           'labels'      => array(
               'name'          => __('Departments'),
               'singular_name' => __('Department'),
           ),
           'public'      => true,
           'has_archive' => true,
           'rewrite'     => array( 'slug' => 'departments' ), // my custom slug
           'supports' => array('title')
       )
    );
}
add_action('init', 'wporg_departments_post_type');

// Get post by category opn homepage
function GetProductsByCategory(){
	global $post;
	$postsdata = '';
    $args = array( 'numberposts' => 5,'post_status' => 'publish', 'category_name' => 'products','orderby' => 'date','order' => 'ASC');
    $posts = get_posts( $args );
    foreach( $posts as $post ): setup_postdata($post); 
    $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' );
	$postsdata.='<div class="product-blog">';
	$postsdata.='<img src="'.$url.'">';
    $postsdata.='<div class="product-overlay-text">';
    $postsdata.='<h4 class="main-title">'.$post->post_title.'</h4>';
    $postsdata.='<a href="'.get_permalink( $post->ID ).'" class="main-btn"> Discover Now</a>';
    $postsdata.='</div>';
    $postsdata.='</div>';
	endforeach;
	return $postsdata;
}

// Get Testimonials by custom post type
function GetManagementTestimonials(){
	$managements = '';
	$args = array(
	    'post_type' => 'wporg_product',
	    'post_status' => 'publish'
	    // Several more arguments could go here. Last one without a comma.
	);
	// Query the posts:
	$obituary_query = new WP_Query($args);
	// Loop through the obituaries:
	while ($obituary_query->have_posts()) : $obituary_query->the_post();
		  $image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()), 'thumbnail' );
		  //$image = get_field('management_image',get_the_ID());
		  $managements.='<div class="item">';
		  $managements.='<div class="row">';
		  $managements.='<div class="col-lg-1"></div>';
	 	  $managements.='<div class="col-lg-4">';
	 	  $managements.='<div class="testimonial-content">';
	 	  $managements.='<img src="'.$image.'"></div></div>';
	 	  $managements.='<div class="col-lg-6"><div class="sub-content"><div class="text-d">';
	 	  $managements.='<h2 class="person-name">'.get_the_title().'</h2>';
	 	  $managements.=get_the_content();
	 	  $managements.='<a href="#" class="btn-red">Read More </a>';
	 	  $managements.='</div></div></div><div class="col-lg-1"></div></div></div>';
	endwhile;
	// Reset Post Data
	wp_reset_postdata();
	return $managements;
}

// Get Testimonials by custom post type
function GetClientLogos(){
	$clients = '';
	$args = array(
	    'post_type' => 'wporg_client',
	    'post_status' => 'publish'
	    // Several more arguments could go here. Last one without a comma.
	);
	// Query the posts:
	$obituary_query = new WP_Query($args);
	// Loop through the obituaries:
	while ($obituary_query->have_posts()) : $obituary_query->the_post();
		$image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()), 'thumbnail' );
		$clients.='<div class="item">';
	 	$clients.='<img src="'.$image.'">';
	 	$clients.='</div>';
	endwhile;
	// Reset Post Data
	wp_reset_postdata();
	return $clients;
}

// Get Banner Sliders by custom post type
function GetBannerSliders(){
	$banners = '';
	$args = array(
	    'post_type' => 'wporg_slider',
	    'post_status' => 'publish'
	    // Several more arguments could go here. Last one without a comma.
	);
	// Query the posts:
	$obituary_query = new WP_Query($args);
	// Loop through the obituaries:
	while ($obituary_query->have_posts()) : $obituary_query->the_post();
		$image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()), 'full' );
		$banners.='<div class="item">';
	 	$banners.='<img src="'.$image.'">';
	 	$banners.='<div class="container overlay-text">';
	 	$banners.=get_the_content();
	 	$banners.='</div></div>';
	endwhile;
	// Reset Post Data
	wp_reset_postdata();
	return $banners;
}

// primary menu example @ https://digwp.com/2011/11/html-formatting-custom-menus/
function clean_primary_menus() {
	$menu_name = 'top'; // specify custom menu slug
	if (($locations = get_nav_menu_locations()) && isset($locations[$menu_name])) {
		$menu = wp_get_nav_menu_object($locations[$menu_name]);
		$menu_items = wp_get_nav_menu_items($menu->term_id);

		$menu_list = '';
		$class = 'active';
		$menu_list .= "\t\t\t\t". '<ul class="navbar-nav">' ."\n";
		foreach ((array) $menu_items as $key => $menu_item) {
			/*if($key == 0){
				$class='active';
			}
			else{
				$class='';
			}*/
			$title = $menu_item->title;
			$url = $menu_item->url;
			$menu_list .= "\t\t\t\t\t". '<li class="nav-item"><a class="nav-link"  href="'. $url .'">'. $title .'</a></li>' ."\n";
		}
		$menu_list .= "\t\t\t\t". '</ul>' ."\n";
		$menu_list .= '';
	} else {
		// $menu_list = '<!-- no list defined -->';
	}
	return $menu_list;
}

// Get 5 products by category
function GetPostsByCategory(){
	//global $post;
	$postsdata = '';
    $args = array( 'numberposts' => 5,'post_status' => 'publish', 'category_name' => 'products','orderby' => 'date','order' => 'ASC');
    $posts = get_posts( $args );
	$postsdata.='<ul class="footer-link">';
    foreach( $posts as $post ): setup_postdata($post); 
	$postsdata.='<li>';
    $postsdata.='<a href="'.get_permalink( $post->ID ).'">'.$post->post_title.'</a>';
    $postsdata.='</li>';
	endforeach;
    $postsdata.='</ul>';
	return $postsdata;
}

// Get junior team members by category
function GetJuniorTeam($category=null){
	$postsdata = '';
	$cat_id = get_cat_ID($category);
	$args = array(
	    'post_type' => 'wporg_team',
	    'post_status' => 'publish',
	    'cat' => $cat_id
	    // Several more arguments could go here. Last one without a comma.
	);
	// Query the posts:
	$obituary_query = new WP_Query($args);
	// Loop through the obituaries:
	while ($obituary_query->have_posts()) : $obituary_query->the_post();
		$image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()), 'full' );
		$postsdata.='<div class="single-team-member text-center">';
		$postsdata.='<div class="member-img">';
		$postsdata.='<img src="'.$image.'">';
		$postsdata.='</div>';
	    $postsdata.='<div class="member-content">';
	    $postsdata.='<h4 class="member-name">'.get_the_title().'</h4>';
	    $postsdata.='<h4 class="member-designation">'.get_the_content().'</h4>';
	    $postsdata.='</div>';
	    $postsdata.='</div>';
	endwhile;
	// Reset Post Data
	wp_reset_postdata();
	return $postsdata;
}

// Get leader team members by category
function GetLeaderTeam($category=null){
	$postsdata = '';
	$cat_id = get_cat_ID($category);
	$args = array(
	    'post_type' => 'wporg_team',
	    'post_status' => 'publish',
	    'cat' => $cat_id
	    // Several more arguments could go here. Last one without a comma.
	);
	// Query the posts:
	$obituary_query = new WP_Query($args);
	// Loop through the obituaries:
	while ($obituary_query->have_posts()) : $obituary_query->the_post();
		$image = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()), 'full' );
		$postsdata.='<div class="single-team-member text-center">';
		$postsdata.='<div class="member-img">';
		$postsdata.='<img src="'.$image.'">';
		$postsdata.='</div>';
	    $postsdata.='<div class="member-content">';
	    $postsdata.='<h4 class="member-name">'.get_the_title().'</h4>';
	    $postsdata.='<h4 class="member-designation">'.get_the_content().'</h4>';
	    $postsdata.='</div>';
	    $postsdata.='</div>';
	endwhile;
	// Reset Post Data
	wp_reset_postdata();
	return $postsdata;
}

function wp_get_positions_by_location() {

	$location = $_POST['location'];
	if($location != 'All') {
		$args = array(
                'post_type'      => 'wporg_positions',
                'meta_query'     => array(
                    array(
                    'key'     => 'location',
                    'value'   => $location,
                    ) 
            )
            );
            $event_query = new WP_Query( $args );

            if($event_query->have_posts()) : 
			$markup = '';
            while($event_query->have_posts()) : 
                $event_query->the_post();
				$id = get_the_ID();
				$dept = get_field('department');
				$panel_title = get_the_title();
				$dept_title = $dept->post_title;
				$panel_experience = get_field('experience');
				$panel_content = get_the_content();
				$markup .= '<div class="panel panel-default">
							
                            <div class="panel-heading" id="headingOne">
                                <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-target="#collapse-'. $id .'" aria-expanded="true" aria-controls="collapse-'. $id .'">
                                        <i class="more-less fa fa-plus"></i>
                                        '. $panel_title .'
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse-'. $id .'" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="panel-body">
                                    <a class="btn-red apply_btn" data-button-label="Apply Now" data-toggle="modal" data-target="#apply-form">
                                        <span class="button__label form_target" data-opening="'. $panel_title .'">Apply Now</span>
                                    </a>
                                    <h5 class="job-position-title">Department : 
                                        <span>'. $dept_title .'</span>
                                    </h5>
                                    <h5 class="job-position-title">Experience :
                                        <span>'. $panel_experience .'</span>
                                    </h5>
                                    <h5 class="job-position-title">Job Description : </h5>
                                    '. $panel_content .'
                                </div>
                            </div>
                        </div>';
                endwhile;
				echo $markup;
            else: 
                echo "Sorry, currently there are no open positions.";
            endif;
			exit();
	} 
	else {
		$args = array(
            'post_type'      => 'wporg_positions',
			'posts_per_page' => '-1'
        );
        $event_query = new WP_Query( $args );

        if($event_query->have_posts()) : 
		$markup = '';
        while($event_query->have_posts()) : 
            $event_query->the_post();
			$id = get_the_ID();
			$dept = get_field('department');
			$panel_title = get_the_title();
			$dept_title = $dept->post_title;
			$panel_experience = get_field('experience');
			$panel_content = get_the_content();
			$markup .= '<div class="panel panel-default">
						
                        <div class="panel-heading" id="headingOne">
                            <h4 class="panel-title">
                                <a class="collapsed" role="button" data-toggle="collapse" data-target="#collapse-'. $id .'" aria-expanded="true" aria-controls="collapse-'. $id .'">
                                    <i class="more-less fa fa-plus"></i>
                                    '. $panel_title .'
                                </a>
                            </h4>
                        </div>
                        <div id="collapse-'. $id .'" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="panel-body">
                                <a class="btn-red apply_btn" data-button-label="Apply Now" data-toggle="modal" data-target="#apply-form">
                                    <span class="button__label form_target" data-opening="'. $panel_title .'">Apply Now</span>
                                </a>
                                <h5 class="job-position-title">Department : 
                                    <span>'. $dept_title .'</span>
                                </h5>
                                <h5 class="job-position-title">Experience :
                                    <span>'. $panel_experience .'</span>
                                </h5>
                                <h5 class="job-position-title">Job Description : </h5>
                                '. $panel_content .'
                            </div>
                        </div>
                    </div>';
            endwhile;
			echo $markup;
        else: 
            echo "Sorry, currently there are no open positions.";
        endif;
		exit();
	}
	
}

add_action("wp_ajax_wp_get_positions_by_location", "wp_get_positions_by_location");
add_action("wp_ajax_nopriv_wp_get_positions_by_location", "wp_get_positions_by_location");