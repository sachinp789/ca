<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'capatel');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'l_;|p58%|owOxso1<E`yG*FpXinkZC[TJn8MWE,voGg:b|_u;tc}?<RF]z-@i>gX');
define('SECURE_AUTH_KEY',  'BA{}s`f!Jz&3>WHN{]J`vtxM^KZ~Wt$%pFE^*OysmnLGrZH+/N1J)e|sz<z.=#]4');
define('LOGGED_IN_KEY',    ']8B<M>>1, Q>FI_~~?cY/*Ca`oSYB9.)3]D5Zf4wL$>{3jw+&;K8r}<G-yT:S?l!');
define('NONCE_KEY',        'w{Mg3h+CBuL9y,K%I_TG%yoc*bQ|jc8SK:9 *FJURAyzLKVSs|$;kako4SwNo])F');
define('AUTH_SALT',        'R9/lC|$Z1|Nn$TI2fl;TqxjV~d?<qBn%|;##HBKycvcB9fkg7d2m#5B%>uw>]}MW');
define('SECURE_AUTH_SALT', 'MQ6V3A@i1xR ?.t(gZ}&!~WUjfg@<#G8LAkGh-1ii*rsQ{v-l(yz|at3@MbU<(d*');
define('LOGGED_IN_SALT',   'CqJD_Mt}MqbA[|Khj7oar<Pki%VQesYWF,]S*q~|;B~!X;]QeKfrO96UG1O?ZY<M');
define('NONCE_SALT',       'O4mv)i04nw/Edy+v=@=&yAPPq}$$S|)g!,b^_z?PQpA@=S-h :xr9@xgg;Z`X}b`');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'cap_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
